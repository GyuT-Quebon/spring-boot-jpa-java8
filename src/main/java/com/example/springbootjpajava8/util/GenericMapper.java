package com.example.springbootjpajava8.util;

public interface GenericMapper <Dto, Entity> {
    Dto toDto(Entity entity);
    Entity toEntity(Dto dto);
}
