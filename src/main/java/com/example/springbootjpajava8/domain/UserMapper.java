package com.example.springbootjpajava8.domain;

import org.mapstruct.BeanMapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.example.springbootjpajava8.util.GenericMapper;

public interface UserMapper extends GenericMapper<UserDTO, User> {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    User patch(UserDTO dto, @MappingTarget User user);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
    User put(UserDTO dto, @MappingTarget User user);
}
