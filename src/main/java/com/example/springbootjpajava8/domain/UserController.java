package com.example.springbootjpajava8.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<UserDTO> create(@RequestBody UserDTO userDTO){
        //TODO return userService.create(userDTO), toDTO
        return ResponseEntity.ok(new UserDTO());
    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> findAll(){
        //TODO return userService.findAll(), toDTO
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDTO> findById(@PathVariable Long id){
        //TODO return userService.findById(id), toDTO
        return ResponseEntity.ok().build();
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity<UserDTO> patch(@PathVariable Long id, @RequestBody UserDTO userDTO){
        //TODO return userService.patch(id, userDTO), toDTO
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UserDTO> put(@PathVariable Long id, @RequestBody UserDTO userDTO){
        //TODO return userService.put(id, userDTO), toDTO
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        //TODO return userService.delete(id)
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
}
