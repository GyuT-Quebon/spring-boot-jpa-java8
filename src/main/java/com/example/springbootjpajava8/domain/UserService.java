package com.example.springbootjpajava8.domain;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper mapper = Mappers.getMapper(UserMapper.class);

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public User create(UserDTO userDTO){
        return userRepository.save(mapper.toEntity(userDTO));
    }

    @Transactional(readOnly = true)
    public User get(Long id) throws Exception {
        return userRepository.findById(id).orElseThrow(Exception::new);
    }

    @Transactional
    public User put(Long id, UserDTO userDTO) throws Exception {
        return userRepository.save(mapper.put(userDTO, get(id)));
    }

    @Transactional
    public User patch(Long id, UserDTO userDTO) throws Exception {
        return userRepository.save(mapper.patch(userDTO, get(id)));
    }

    @Transactional
    public void delete(Long id) throws Exception {
        userRepository.delete(get(id));
    }
}
