package com.example.springbootjpajava8.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.example.springbootjpajava8.model.BaseEntity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(
    name = "users",
    indexes = {
        @Index(columnList = "email", unique = true, name = "index_unique_email")
    }
)
public class User extends BaseEntity {
    @Email
    private String email;

    @NotBlank
    @Column(length = 1024)
    private String password;

    @NotBlank
    @Column(length = 32)
    private String name;

    @NotBlank
    @Column(length = 32)
    private String mobile;

    private String address;

    @Enumerated(EnumType.STRING)
    private Role role = Role.NONE;

    @Builder
    public User(String email, String password, String name, String mobile, String address, Role role){
        this.email = email;
        this.password = password;
        this.name = name;
        this.mobile = mobile;
        this.address = address;
        this.role = role;
    }

    public enum Role {
        NONE, USER, DEVELOPER, MANAGER, ADMIN
    }
}