package com.example.springbootjpajava8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJpaJava8Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJpaJava8Application.class, args);
    }

}
